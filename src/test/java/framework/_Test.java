package framework;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;

import static framework.BrowserFactory.Browser.CHROME_LOCAL;
// commenting out this unused import
//import static framework.BrowserFactory.Browser.CHROME_SAUCE;
import static framework.BrowserFactory.Browser.CHROME_SAUCE;
import static framework.BrowserFactory.createBrowser;

public class _Test {

    protected static WebDriver driver;
    protected static Logger logger = LoggerFactory.getLogger(_Test.class.getName());


    @BeforeAll
    public static void setUp() throws MalformedURLException {
        logger = LoggerFactory.getLogger(_Test.class);
        logger.info("Starting Selenium webdriver framework");

        logger.debug("Creating driver");
        driver = createBrowser(CHROME_SAUCE);

        logger.debug("setting window size");
        driver.manage().window().setSize(new Dimension(1280, 1024));

        logger.debug("Visiting website");
        driver.get(Configuration.getProperty("baseUrl"));

        logger.info("Starting frontend tests");
    }

    @BeforeEach
    public void testSetUp() {

    }

    @AfterEach
    public void testTearDown() {

    }

    @AfterAll
    public static void tearDown() {
        driver.quit();
        logger.info("End of frontend tests");
    }

}
