package framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import static framework.Configuration.getProperty;

class BrowserFactory {

    private static final Logger logger = LoggerFactory.getLogger(BrowserFactory.class);

    public enum Browser {
        CHROME_LOCAL,
        CHROME_SAUCE,

        INTERNET_EXPLORER_LOCAL,
        INTERNET_EXPLORER_SAUCE,
    }

    static WebDriver createBrowser(Browser browser) throws MalformedURLException {
        switch (browser) {

            case CHROME_LOCAL:
                return createChromeBrowser();
            case CHROME_SAUCE:
                return createSauceLabsChromeBrowser();

            case INTERNET_EXPLORER_LOCAL:
                return createInternetExplorerDriver();
            case INTERNET_EXPLORER_SAUCE:
                return createSauceLabsInternetExplorerDriver();

            default:
                throw new NotImplementedException("Unknown browser name supplied.");
        }
    }

    // Local browsers
    private static WebDriver createChromeBrowser() {
        WebDriverManager.chromedriver().setup();
        return webDriverWithListener(new ChromeDriver(new ChromeOptions()));
    }

    private static WebDriver createInternetExplorerDriver() {
        WebDriverManager.iedriver().setup();
        return webDriverWithListener(new InternetExplorerDriver(new InternetExplorerOptions()));
    }

    // Saucelabs browsers
    private static WebDriver createSauceLabsInternetExplorerDriver() throws MalformedURLException {
        return createSauceLabsDriver("IE", "11.0", "Windows 10");
    }

    private static WebDriver createSauceLabsChromeBrowser() throws MalformedURLException {
        return createSauceLabsDriver("Chrome", "75.0", "macOS 10.14");
    }

    // Support for local browsers
    private static EventFiringWebDriver webDriverWithListener(WebDriver driver) {
        return new EventFiringWebDriver(driver).register(new BrowserEventListener());
    }

    // Support for Saucelabs browsers
    private static WebDriver createSauceLabsDriver(String browserName, String browserVersion, String platformNameAndVersion) throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("username", getProperty("sauceLabsUsername"));
        capabilities.setCapability("accessKey", getProperty("sauceLabsKey"));
        capabilities.setCapability("platform", platformNameAndVersion);
        capabilities.setCapability("browserName", browserName);
        capabilities.setCapability("version", browserVersion);
        capabilities.setCapability("name", "Valori Frameworkshop Test");
        return webDriverWithListener(new RemoteWebDriver(new URL(getProperty("sauceUrl")), capabilities));
    }

}
