package framework;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

class Configuration {

    static String getProperty(String propertyName) {
        Properties properties = new Properties();
        String propertyValue = null;

        try (InputStream input = Configuration.class.getClassLoader().getResourceAsStream(
                "config.properties")) {
            properties.load(Objects.requireNonNull(input));
            propertyValue = properties.getProperty(propertyName);
        } catch (IOException ioex) {
            System.out.println("Problems opening properties:" + ioex);
        }
        return propertyValue;
    }

}