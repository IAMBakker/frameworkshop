package tests;

import framework._Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HomePageTest extends _Test {

    @Test
    void should_visit_homepage() {
        Assertions.assertEquals("Valori Automation Practice Shop!", driver.getTitle(),
                "Homepage Title does not match expected value.");
    }
}
